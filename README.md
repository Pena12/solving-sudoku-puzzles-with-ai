# Solving Sudoku Puzzles with AI

A purpose solver that solves sudoku puzzles using AI. The solver uses depth-first search algorithm combined with human techniques such as hidden singles, naked pairs etc. to solve given puzzles within a dataset.

**Installation:**
1.	Check that python is installed. If it is already installed, then check the version running on the system just to make sure it is the most recent. This can be done by typing: **python --version** on the terminal.  If it is not installed, then instructions on how to download it can be found here: https://realpython.com/installing-python/

2.	Make sure pip is also installed on the system. This can be confirmed by using the command **pip --version**. If not, then open a new terminal and type the following command: **sudo apt-get install python3-pip** (Python version 3)

3.	Tkinter should be included with python. In the case that it is not, type the following command on the terminal: **sudo apt-get install python3-tk**. This command installs Tkinter for python version 3.

**Executing the program:**
1.	CD into the directory that contains the  project files. 
2.	Open a terminal inside the directory and create a virtual environment:       **venv env** (for python3) or **virtualenv env** (for python2).
3.	Activate the virtual environment: **source env/bin/activate**
4.	With the virtual environment activated, install numpy using: **pip install –U numpy**
5.	With the virtual environment still activated, run the main file with the command: **python3 main.py**

A GUI will open in a new window and will prompt for the following:
* Select the dataset to use. Click the open button and choose one of the five available datasets (text files).
* Select the solving method. Click the box and select the desired method.
* Next, set the node expansion limit. This can be any number starting from 1.
* Finally, click the start button and the solver will begin to solve the puzzles in the dataset.
