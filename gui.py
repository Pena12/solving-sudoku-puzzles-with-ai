import tkinter
from tkinter import messagebox, filedialog
from tkinter import *
import os
import load_dataset as ld
import random
from dfs import DFS
from sudoku import Sudoku
import numpy as np


def main():
    root = tkinter.Tk()
    
    root.title("Pena's Sudoku Solver")

    alg_selected = StringVar(root)
    expand_limit = StringVar(root)
    choices = {"DFS + NS": 0,
               "DFS + NS + NP": 1,
               "DFS + NS + NP + NT": 2,
               "DFS + NS + NP + NT + HS": 3,
               "DFS + NS + NP + NT + HS + HP": 4,
               "DFS + NS + NP + NT + HS + HP + HT": 5,
               "DFS + HS + NP + NT + HS + HP + HT + XW": 6,
               "DFS + HS + NP + NT + HS + HP + HT + XW + SF": 7}
    root.alg = 0
    root.exp = 0
    alg_selected.set("DFS + NS")

    # ===============================DATASET====================================================================

    l1 = Label(root, text="Select dataset file")
    l1.grid(row=0, column=0, ipadx=5, ipady=5, padx=5, pady=5, sticky='W')

    l2 = Label(root, text="", borderwidth=2, width=60, relief="sunken", anchor='w')
    l2.grid(row=0, column=1, ipadx=5, ipady=5, padx=5, pady=5, sticky='W')

    b1 = Button(root, width=10, height=1, bd=5, text="Open", command=lambda: open_dataset_file(root, l2))
    b1.grid(row=0, column=2, ipadx=5, ipady=5, padx=5, pady=5)

    # ===============================ALGORITHM==================================================================

    l5 = Label(root, text="Select optimization algorithm")
    l5.grid(row=2, column=0, ipadx=5, ipady=5, padx=5, pady=5, sticky='W')

    popup = OptionMenu(root, alg_selected, *choices)
    popup.config(width=60, anchor='w')
    popup.grid(row=2, column=1, ipadx=5, ipady=5, padx=5, pady=5, sticky='W')

    # ================================NODES=====================================================================

    l6 = Label(root, text="Maximum number of expanded nodes")
    l6.grid(row=3, column=0, ipadx=5, ipady=5, padx=5, pady=5, sticky='W')

    entry = Entry(root, width=5, bd=3, justify="center", textvariable=expand_limit)
    entry.grid(row=3, column=1, ipadx=5, ipady=5, padx=5, pady=5, sticky='W')

    # =================================START====================================================================

    # Put the Start button
    b3 = Button(root, width=20, height=1, bd=5, text="Start", command=lambda: checkData())
    b3.grid(row=5, column=1, ipadx=5, ipady=5, padx=5, pady=5, columnspan=2, sticky='E')

    def check_number(s):
        try:
            int(s)
            return True
        except ValueError:
            return False

    def checkData():
        if not check_number(expand_limit.get()):
            messagebox.showerror("Value Error", "Node limit must be a number")
        else:
            root.exp = int(expand_limit.get())
            start(root)

    # Trace the user selection of algorithm
    def trace_selected_algorithm(*args):
        root.alg = choices[alg_selected.get()]

    alg_selected.trace('w', trace_selected_algorithm)

    root.mainloop()


def open_dataset_file(root, labelwidget: Label):
    filename = filedialog.askopenfilename(initialdir=os.getcwd(), title="Select dataset file",
                                          filetypes=[("Text file", "*.txt")])

    root.dataset_name = filename
    labelwidget["text"] = filename.split('/')[-1]

total_solved = 0

#number of puzzles in dataset can't be less than limit
limit = 20

def start(root, limit=limit):
    data, blocks, block_label = ld.main(root.dataset_name)


    global total_solved
    
    #randomly selects n puzzles from dataset
    rand_data = random.sample(data, limit)

    for x1 in rand_data:
        sudoku = Sudoku(x1, block_label)

        dfs = DFS(max_nodes=root.exp)
        solved = dfs.run(sudoku, root.alg)

        if solved:
            total_solved += 1


    root.destroy()

    print("\n {}/{} puzzles solved".format(total_solved, limit))
