def uni(s):
    return chr(int(s, 16))


def fancy_grid_line(start, norm, lcross, hcross, end):
    start, norm, lcross, hcross, end = (uni(start), uni(norm),
                                        uni(lcross), uni(hcross), uni(end))
    return (start +
            ((norm * 3 + lcross) * 2
             + norm * 3 + hcross) * 2
            + (norm * 3 + lcross) * 2
            + norm * 3 + end)


TOP_LINE = fancy_grid_line('2554', '2550', '2564', '2566', '2557')
THIN_MID_LINE = fancy_grid_line('255f', '2500', '253c', '256b', '2562')
THICK_MID_LINE = fancy_grid_line('2560', '2550', '256a', '256c', '2563')
BOTTOM_LINE = fancy_grid_line('255a', '2550', '2567', '2569', '255d')


def display_sudoku_grid(grid, ID=None, first_index=0):
    dvbar = uni('2551')
    indent = "    "
    filled = 0
    for row in grid:
        for c in row:
            if c > 0: filled += 1
    if ID:
        print("    ID: {}      ({} cells filled)".format(ID, filled))
    print(indent + TOP_LINE)
    for row in range(9):
        print(indent + dvbar, end="")
        for col in range(9):
            # print(row)
            if col % 3 == 2:
                barstr = dvbar
            else:
                barstr = uni('2502')
            num = grid[row + first_index][col + first_index]
            if num != '':
                print(" " + str(num) + " " + barstr, end="")
            else:
                print("   " + barstr, end="")
        print()
        if row == 8:
            print(indent + BOTTOM_LINE)
        elif row % 3 == 2:
            print(indent + THICK_MID_LINE)
        else:
            print(indent + THIN_MID_LINE)