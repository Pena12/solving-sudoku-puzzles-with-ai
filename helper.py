# coding=utf-8
import numpy as np
from copy import deepcopy
import display_sudoku as disp


class Helper:
    def get_all_possibility(self, table: list):
        sz = np.array(table).shape

        possibilities = [[[] for _ in range(sz[0])] for _ in range(sz[1])]

        for i in range(sz[0]):
            for j in range(sz[1]):
                if table[i][j] == '':
                    row_pos = self.get_row_possibility(table, i)
                    col_pos = self.get_column_possibility(table, j)
                    block_pos = self.get_block_possibility(table, i, j)

                    final_pos = [num for num in [i for i in range(1, 10)] if
                                 num in row_pos and num in col_pos and num in block_pos]
                    possibilities[i][j] = final_pos

        return possibilities

    def get_column_possibility(self, table: list, col_idx: int):
        sz = np.array(table).shape

        num = [i for i in range(1, 10)]
        col_elem = [table[idx][col_idx] for idx in range(sz[0])]

        free_num = [n for n in num if n not in col_elem]

        return free_num

    def get_row_possibility(self, table: list, row_idx: int):
        sz = np.array(table).shape

        num = [i for i in range(1, 10)]
        row_elem = [table[row_idx][idx] for idx in range(sz[1])]

        free_num = [n for n in num if n not in row_elem]

        return free_num

    def get_block_possibility(self, table: list, row_idx: int, col_idx: int):
        sz = np.array(table).shape

        num = [i for i in range(1, 10)]
        block_elem = [table[i][j] for i in range(sz[0]) for j in range(sz[1]) if
                      self.get_block_label(row_idx, col_idx) == self.get_block_label(i, j)]

        free_num = [n for n in num if n not in block_elem]

        return free_num

    def get_block_label(self, row: int, col: int):
        block = [[1, 1, 1, 2, 2, 2, 3, 3, 3],
                 [1, 1, 1, 2, 2, 2, 3, 3, 3],
                 [1, 1, 1, 2, 2, 2, 3, 3, 3],
                 [4, 4, 4, 5, 5, 5, 6, 6, 6],
                 [4, 4, 4, 5, 5, 5, 6, 6, 6],
                 [4, 4, 4, 5, 5, 5, 6, 6, 6],
                 [7, 7, 7, 8, 8, 8, 9, 9, 9],
                 [7, 7, 7, 8, 8, 8, 9, 9, 9],
                 [7, 7, 7, 8, 8, 8, 9, 9, 9]]

        return block[row][col]

    def solved(self, table: list):
        for row in table:
            if '' in row:
                return False

        return True

    def empty_possibility(self, table: list):
        possibilities = self.get_all_possibility(table)
        empty = True
        for row in possibilities:
            for col in row:
                if col:
                    empty = False

        return empty

    def modify_table(self, table: list, step: list):
        sudoku = deepcopy(table)

        for i in range(len(step[1])):
            sudoku[step[0][i][0]][step[0][i][1]] = step[1][i]

        return sudoku

    def show_sudoku(self, table: list):
        sz = np.array(table).shape

        fig, axs = plt.subplots(1, 1)
        cell_text = [x for x in table]
        axs.axis('tight')
        axs.axis('off')
        the_table = axs.table(cellText=cell_text, loc='center', colWidths=[0.05 for _ in range(sz[1])],
                              cellLoc='center')

        col_color1 = [i for i in range(2, sz[1], 6)]
        col_color2 = [i for i in range(5, sz[1], 6)]

        for key, cell in the_table.get_celld().items():
            if key[1] + 1 in col_color1 or key[1] + 2 in col_color1 or key[1] in col_color1:
                if key[0] + 1 in col_color1 or key[0] + 2 in col_color1 or key[0] in col_color1:
                    cell.set_facecolor("#CECECE")

            if key[1] + 1 in col_color2 or key[1] + 2 in col_color2 or key[1] in col_color2:
                if key[0] + 1 in col_color2 or key[0] + 2 in col_color2 or key[0] in col_color2:
                    cell.set_facecolor("#CECECE")

        plt.show()

    def show_grid(self, sudoku):
        disp.display_sudoku_grid(sudoku.table)
