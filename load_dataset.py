import numpy as np


def main(filename: str):
    data = []
    blocks = []
    block_label = [[0 for _ in range(9)] for _ in range(9)]

    fp = open(filename, mode='r')
    line = fp.readline()

    while line:
        line = line.replace('\n', '')
        spl = [int(i) if i != '.' else '' for i in line]
        spl = list(chunks(spl, 9))

        data.append(spl)
        blocks.append(block_split(spl, 3, block_label))

        line = fp.readline()

    return data, blocks, block_label

def chunks(l: list, n: int):
    for i in range(0, len(l), n):
        yield l[i:i + n]

def block_split(l: list, n: int, block_label: list):
    block_data = []
    sz = np.array(l).shape

    label = 1
    for lim in range(0, sz[0], n):
        for i in range(0, sz[1], n):
            block = []

            for row in range(lim, lim + n):
                for col in range(i, i + n):
                    block_label[row][col] = label
                    block.append(l[row][col])

            block = list(chunks(block, 3))
            block_data.append(block)

            label += 1

    return block_data
